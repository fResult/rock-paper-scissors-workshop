import React, { Suspense } from 'react'
import './App.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Layout from './components/Layout'

const routes = [
  { path: '/', component: React.lazy(() => import('./pages/Home')) },
  {
    path: '/original',
    component: React.lazy(() => import('./pages/Original'))
  },
  { path: '/advanced', component: React.lazy(() => import('./pages/Advanced')) }
]

function App() {
  return (
    <div className="bg-gradient-to-b from-bg-1 to-bg-2 font-semibold tracking-widest">
      <Router>
        <Layout>
          <Suspense fallback="">
            <Switch>
              {routes.map(({ path, component }) => {
                return (
                  <Route
                    exact={true}
                    key={path}
                    path={path}
                    component={component}
                  />
                )
              })}
            </Switch>
          </Suspense>
        </Layout>
      </Router>
    </div>
  )
}

export default App
