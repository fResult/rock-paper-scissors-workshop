import React from 'react'
import Button from './index'

function ButtonOutline({ className, children, style, onClick }: ButtonProps) {
  return (
    <Button
      className={`${
        className ? className : ''
      } bg-transparent border-header-outline border-4 p-6 hover:bg-opacity-5 hover:bg-white rounded-lg`}
      {...{ style, onClick }}
    >
      {children}
    </Button>
  )
}

export default ButtonOutline
