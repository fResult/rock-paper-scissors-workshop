import React from 'react'
import Button from './index'

function ButtonWhite({ className, children, style, onClick }: ButtonProps) {
  return (
    <Button
      className={`${
        className ? className : ''
      } bg-white px-6 hover:text-red-800 text-xs font-bold tracking-widest text-dark-text rounded-lg`}
      {...{ style, onClick }}
    >
      {children}
    </Button>
  )
}

export default ButtonWhite
