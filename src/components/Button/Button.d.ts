type ButtonProps = {
  className?: string
  children?: React.ReactNode
  style?: React.CSSProperties
  onClick?: (e: any) => void
}
