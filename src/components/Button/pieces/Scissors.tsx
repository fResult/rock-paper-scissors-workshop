import React from 'react'
import ButtonCircle from '../ButtonCircle'
import scissors from '../../../images/icon-scissors.svg'

type PieceButtonProps = {
  className?: string
  style?: React.CSSProperties,
  onClick?: (e: any) => void
}

const BtnScissors = ({ className, style, onClick }: ButtonProps) => {
  return (
    <ButtonCircle
      className={`${className ? className : ''} border-scissors-1`}
      style={{
        boxShadow: '#B1770B 0 4px 2px, inset #5B5B74 0 4px 1px',
        ...style
      }}
      onClick={onClick}
    >
      <img src={scissors} alt="Scissors" className="w-full" />
    </ButtonCircle>
  )
}

export default BtnScissors
