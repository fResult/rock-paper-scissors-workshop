import React from 'react'
import ButtonCircle from '../ButtonCircle'
import spock from '../../../images/icon-spock.svg'

const BtnSpock = ({ className, style, onClick }: ButtonProps) => {
  return (
    <ButtonCircle
      className={`${className ? className : ''} border-spock-1`}
      style={{
        boxShadow: '#308B9B 0 4px 2px, inset #5B5B74 0 4px 1px',
        ...style
      }}
      onClick={onClick}
    >
      <img src={spock} alt="Spock" className="w-full" />
    </ButtonCircle>
  )
}

export default BtnSpock
