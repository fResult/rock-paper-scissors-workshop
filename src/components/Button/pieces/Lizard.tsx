import React from 'react'
import ButtonCircle from '../ButtonCircle'
import lizard from '../../../images/icon-lizard.svg'

const BtnLizard = ({ className, style, onClick }: ButtonProps) => {
  return (
    <ButtonCircle
      className={`${className ? className : ''} border-lizard-1`}
      style={{
        boxShadow: '#623BAA 0 4px 2px, inset #5B5B74 0 4px 1px',
        ...style
      }}
      onClick={onClick}
    >
      <img src={lizard} alt="Lizard" className="w-full" />
    </ButtonCircle>
  )
}

export default BtnLizard
