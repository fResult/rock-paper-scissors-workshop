import React from 'react'
import ButtonCircle from '../ButtonCircle'
import rock from '../../../images/icon-rock.svg'

const BtnRock = ({ className, style, onClick }: ButtonProps) => {
  return (
    <ButtonCircle
      className={`${className ? className : ''} border-rock-1`}
      style={{
        boxShadow: '#A5233B 0 4px 2px, inset #5B5B74 0 4px 1px',
        ...style
      }}
      onClick={onClick}
    >
      <img src={rock} alt="Rock" className="w-full" />
    </ButtonCircle>
  )
}

export default BtnRock
