import React from 'react'
import ButtonCircle from '../ButtonCircle'
import paper from '../../../images/icon-paper.svg'

const BtnPaper = ({ className, style, onClick }: ButtonProps) => {
  return (
    <ButtonCircle
      className={`${className ? className : ''} border-paper-1`}
      style={{
        boxShadow: '#364CB7 0 4px 2px, inset #5B5B74 0 4px 1px',
        ...style
      }}
      onClick={onClick}
    >
      <img src={paper} alt="Paper" className="w-full" />
    </ButtonCircle>
  )
}

export default BtnPaper
