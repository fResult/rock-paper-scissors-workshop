import React from 'react'
import Button from './index'

function ButtonCircle({ className, children, style, onClick }: ButtonProps) {
  return (
    <Button
      className={`${
        className ? className : ''
      } bg-white p-6 hover:bg-opacity-5  rounded-full active:bg-white focus:bg-white`}
      {...{
        style: {
          height: 130,
          width: 130,
          borderWidth: '15px 15px 12px 15px',
          ...style
        },
        onClick
      }}
    >
      {children}
    </Button>
  )
}

export default ButtonCircle
