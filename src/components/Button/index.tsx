import React from 'react'

function Button({ className, children, style, onClick }: ButtonProps) {
  return (
    <button
      className={`${className ? className : ''} focus:outline-none`}
      {...{ style, onClick }}
    >
      {children}
    </button>
  )
}

export default Button
