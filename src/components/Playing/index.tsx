import React from 'react'
import triangle from '../../images/bg-triangle.svg'
// import pentagon from '../../images/bg-pentagon.svg'
import BtnScissors from '../Button/pieces/Scissors'
import BtnRock from '../Button/pieces/Rock'
import BtnPaper from '../Button/pieces/Paper'
import useWindowSize from '../../cores/hooks/useWindowSize'
// import BtnLizard from '../Button/pieces/Lizard'
// import BtnSpock from '../Button/pieces/Spock'
// import pentagon from '../../images/bg-pentagon.svg'

type PlayingProps = {}

function Playing(props: PlayingProps) {
  const size = useWindowSize()
  const isDesktopSize = (size.width as number) >= 680

  return (
    <div className="playing-zone text-center">
      <div
        className="relative mt-6"
        style={{
          background: `url(${triangle}) no-repeat center`,
          minHeight: 300,
          backgroundSize: isDesktopSize ? 235 : 180
        }}
      >
        <BtnPaper className="absolute" style={{ width: 134, height: 134 }} />
        <BtnScissors className="absolute" style={{ width: 134, height: 134, left: 150 }} />
        <BtnRock className="absolute" style={{ width: 134, height: 134, top: 120, left: 75 }} />
        {/*<BtnLizard />*/}
        {/*<BtnSpock />*/}
      </div>
    </div>
  )
}

export default Playing
