import React from 'react'
import original from '../../images/logo.svg'
import advance from '../../images/logo-bonus.svg'
import useWindowSize from '../../cores/hooks/useWindowSize'

type ScoreBarProps = {
  type: 'original' | 'advanced'
}

function ScoreBar(props: ScoreBarProps) {
  const { type } = props

  const size = useWindowSize()
  const isDesktopSize = (size.width as number) >= 680

  return (
    <div className="border-4 rounded-lg border-header-outline flex items-center justify-between p-4">
      <img
        src={type === 'original' ? original : advance}
        style={{ height: isDesktopSize ? 90 : 45 }}
        alt={type}
      />
      <div
        className="tracking-widest bg-white rounded-lg flex flex-col justify-around items-center"
        style={{
          height: isDesktopSize ? 110 : 70,
          width: isDesktopSize ? 150 : 80
        }}
      >
        <span className="text-sm block text-dark-text">SCORE</span>
        <span className="text-4xl block text-score-text">12</span>
      </div>
    </div>
  )
}

export default ScoreBar
