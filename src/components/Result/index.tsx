import React from 'react'
import ButtonWhite from '../Button/ButtonWhite'

const Result = () => {
  return (
    <div className="text-center">
      <span className="text-white" style={{ fontSize: 40 }}>
        YOU WIN
      </span>
      <ButtonWhite style={{ width: 220, height: 48 }}>PLAY AGAIN</ButtonWhite>
    </div>
  )
}

export default Result
