import React from 'react'
// import Header from './Header'
import Content from './Content'

type LayoutProps = {
  children: React.ReactNode
}

function Layout({ children }: LayoutProps) {
  return (
    <div className="layout min-h-screen">
      {/*<Header />*/}
      <Content>{children}</Content>
    </div>
  )
}

export default Layout
