import React from 'react'

function Header() {
  return (
    <header
      className="header flex justify-between items-center px-4 sm:px-20 text-light-text dark:text-dark-text shadow-lg"
      style={{ height: 80 }}
    >
      Header
    </header>
  )
}

export default Header
