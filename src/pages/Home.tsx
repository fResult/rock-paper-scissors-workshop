import React from 'react'
import { useHistory } from 'react-router-dom'
import bonus from '../images/logo-bonus.svg'
import original from '../images/logo.svg'
import ButtonOutline from '../components/Button/ButtonOutline'

function Home() {
  const history = useHistory()
  return (
    <div
      className="home flex flex-col justify-center items-center gap-16"
      style={{ height: `calc(${window.outerHeight}px - 80px - 32px - 32px` }}
    >
      <ButtonOutline
        style={{ height: 180, width: 300 }}
        onClick={() => history.push('/original')}
      >
        <img src={original} alt="Original" />
      </ButtonOutline>

      <ButtonOutline
        style={{ height: 180, width: 300 }}
        onClick={() => history.push('/advanced')}
      >
        <img src={bonus} alt="Bonus" />
      </ButtonOutline>
    </div>
  )
}

export default Home
