import React from 'react'
import ScoreBar from '../components/ScroreBar'
import Playing from '../components/Playing'
import Result from '../components/Result'

function Advanced() {
  return (
    <div className="advanced-mode">
      <ScoreBar type="advanced" />
      <Playing />
      <Result />
    </div>
  )
}

export default Advanced
