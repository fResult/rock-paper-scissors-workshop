import React from 'react'
import ScoreBar from '../components/ScroreBar'
import Result from '../components/Result'
import Playing from '../components/Playing'

function Original() {
  return (
    <div className="original-mode">
      <ScoreBar type="original" />
      <Playing />
      <Result />
    </div>
  )
}

export default Original
