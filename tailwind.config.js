module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        scissors: {
          '1': '#EC9E0E',
          '2': '#ECA922'
        },
        paper: {
          '1': '#4865F4',
          '2': '#5671F5'
        },
        rock: {
          '1': '#DC2E4E',
          '2': '#DD405D'
        },
        lizard: {
          '1': '#834FE3',
          '2': '#8C5DE5'
        },
        spock: {
          '1': '#40B9CE',
          '2': '#52BED1'
        },
        'dark-text': '#3B4363',
        'score-text': '#2A46C0',
        'header-outline': '#606E85',
        bg: {
          '1': '#1F3756',
          '2': '#141539'
        }
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
